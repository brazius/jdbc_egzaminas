package lt.codeacademy;

import java.sql.Date;

public class Transaction {
    private String account_id_to;
    private String getAccount_id_from;
    private double sum;
    private Date date;

    public Transaction(String account_id_to, String getAccount_id_from, double sum, Date date) {
        this.account_id_to = account_id_to;
        this.getAccount_id_from = getAccount_id_from;
        this.sum = sum;
        this.date = date;
    }

    public String getAccount_id_to() {
        return account_id_to;
    }

    public String getGetAccount_id_from() {
        return getAccount_id_from;
    }

    public double getSum() {
        return sum;
    }

    public Date getDate() {
        return date;
    }
}
