package lt.codeacademy;

public class Account {
    private String account_id;
    private String userId;
    private String accountType;
    private double balance;

//    public Account() {
//    }

    public Account(String account_id, String userId, double balance, String accountType) {
        this.account_id = account_id;
        this.userId = userId;
        this.accountType = accountType;
        this.balance = balance;

    }

    public String getAccount_id() {
        return account_id;
    }

    public String getUserId() {
        return userId;
    }

    public String getAccountType() {
        return accountType;
    }

    public double getBalance() {
        return balance;
    }
}
