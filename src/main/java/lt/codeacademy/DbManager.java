package lt.codeacademy;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class DbManager {
    Print print = new Print();
    Scanner scanner = new Scanner(System.in);

    private Connection createConnection() throws ClassNotFoundException, SQLException {
        Class.forName("org.h2.Driver");
        return DriverManager.getConnection("jdbc:h2:tcp://localhost/~/test", "sa", "");
    }

    public DbManager() {
    }

    public void userRegistration() {
        Connection connection = null;
        try {
            connection = createConnection();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            print.userRegEnterId();
            String userId = scanner.nextLine();
            print.enterName();
            String name = scanner.nextLine();
            print.enterLastName();
            String lastName = scanner.nextLine();
            String password = randomNumberGenerator(4);

            PreparedStatement statement = connection.prepareStatement("INSERT INTO User VALUES ('"
                    + userId + "', '"
                    + name + "', '"
                    + lastName + "', '"
                    + password + "')");
            statement.executeUpdate();
            print.yourPasswordIs(password);
            CreateUserAccount(userId);


        } catch (SQLException e) {
            print.unableToConnectToDb();
            e.printStackTrace();
        }
    }

    private void CreateUserAccount(String userId) {
        try {
            Connection connection = createConnection();
            try {
                connection.setAutoCommit(false);

                try {

                    String debitAccount = "LT" + randomNumberGenerator(20);
                    String kreditAccount = "LT" + randomNumberGenerator(20);
                    PreparedStatement statementDebit = connection.prepareStatement("INSERT INTO Account VALUES ('"
                            + debitAccount + "', '"
                            + userId + "',"
                            + 0);
                    statementDebit.executeUpdate();
                    PreparedStatement statementKredit = connection.prepareStatement("INSERT INTO Account VALUES ('"
                            + kreditAccount + "', '"
                            + userId + "',"
                            + 1000);
                    statementKredit.executeUpdate();
                    connection.commit();
                } catch (SQLException e) {
                    print.unableToConnectToDb();
                    e.printStackTrace();
                } finally {
                    connection.setAutoCommit(true);
                }

            } finally {
                connection.close();
            }
        } catch (SQLException e) {
            print.unableToConnectToDb();
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            print.unableFindDriver();
            e.printStackTrace();
        }
    }

    private String randomNumberGenerator(int numberLenght) {
        Random random = new Random();
        String[] arr = new String[numberLenght];
        String number = "";
        for (int j = 0; j < arr.length; j++) {
            number += (random.nextInt(9));
        }
        return number;
    }

    public List<User> getAllUsers() {
        try {
            Connection connection = createConnection();

            PreparedStatement statement = connection.prepareStatement("SELECT * FROM User");

            ResultSet result = statement.executeQuery();

            List<User> userList = new ArrayList<User>();

            while (result.next()) {
                String userId = result.getString("user_id");
                String name = result.getString("name");
                String lastName = result.getString("last_name");
                String password = result.getString("password");

                User user = new User(userId, name, lastName, password);
                userList.add(user);
            }
            return userList;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return new ArrayList<User>();
    }

    public List<Account> getAllAccounts() {
        try {
            Connection connection = createConnection();

            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Account");

            ResultSet result = statement.executeQuery();

            List<Account> accountList = new ArrayList<Account>();

            while (result.next()) {
                String id = result.getString("id");
                String userId = result.getString("user_id");
                double balance = result.getDouble("balance");
                String accountType = result.getString("account_type");

                Account account = new Account(id, userId, balance, accountType);
                accountList.add(account);
            }

        } catch (SQLException e) {
            print.unableToConnectToDb();
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            print.unableFindDriver();
            e.printStackTrace();
        }
        return new ArrayList<Account>();
    }

    public void moneyDeposit(String userId) {
        try (Connection connection = createConnection()) {
            connection.setAutoCommit(false);
            try {
                DbManager dbManager = new DbManager();
                List<Account> accounts = dbManager.getAllAccounts();
                print.moneyIn();
                print.moneySum();
                double sum = scanner.nextDouble();
                for (Account account : accounts) {
                    if (account.getUserId().equals(userId)) {
                        PreparedStatement statement = connection.prepareStatement("UPDATE Cretid_card SET balance = " + sum + "WHERE accound_id = " + account.getAccount_id());
                        statement.execute();
                        break;
                    }

                }
                connection.commit();
                print.dopositSucces();
            } catch (SQLException e) {
                connection.rollback();
                print.depositError();
            } finally {
                connection.setAutoCommit(true);
            }
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            print.unableToConnectToDb();
            e.printStackTrace();
        }
    } catch(
    SQLException e)

    {
        print.unableToConnectToDb();
        e.printStackTrace();
    } catch(
    ClassNotFoundException e)

    {
        print.unableFindDriver();
        e.printStackTrace();
    }

    public void moneyWithdraw(String userId) {
        try (Connection connection = createConnection()) {
            connection.setAutoCommit(false);
            try {
                DbManager dbManager = new DbManager();
                List<Account> accounts = dbManager.getAllAccounts();
                print.moneyWidraw();
                print.moneySum();
                double sum = scanner.nextDouble();
                for (Account account : accounts) {
                    if (account.getUserId().equals(userId)) {
                        if (sum < account.getBalance() && account.getBalance() > 0) {
                            PreparedStatement statement = connection.prepareStatement("UPDATE Cretid_card SET balance = balance - " + sum + "WHERE accound_id = " + account.getAccount_id());
                            statement.execute();
                            print.widrawOk();
                            break;
                        } else {
                            print.noMoney();
                            break;
                        }
                    }
                }
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                print.widrawNotOk();
            } finally {
                connection.setAutoCommit(true);
            }
        } catch (SQLException e) {
            System.out.println("Unable to connect to db");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("Unable to find driver");
            e.printStackTrace();
        }
    }

    public void moneyTransfer(String userId) {
        try (Connection connection = createConnection()) {
            connection.setAutoCommit(false);
            try {
                DbManager dbManager = new DbManager();
                List<Account> accounts = dbManager.getAllAccounts();
                Scanner scanner = new Scanner(System.in);
                print.enterAccountId();
                String accId = scanner.next();
                for (Account account : accounts) {
                    if (account.getAccount_id().equals(accId)) {
                        for (Account account2 : accounts) {
                            if (account2.getUserId().equals(userId)) {
                                print.acountFound();
                                print.moneySum();
                                double sum = scanner.nextDouble();
                                if (sum < account.getBalance() && account.getBalance() > 0) {
                                    PreparedStatement statementPav = connection.prepareStatement();
                                    statementPav.execute();
                                    PreparedStatement statement = connection.prepareStatement();
                                    statement.execute();
                                    PreparedStatement statement2 = connection.prepareStatement();
                                    statement2.execute();
                                    break;
                                } else {
                                    print.noMoney();
                                    break;
                                }
                            }
                        }
                        break;
                    } 
                }
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                print.transferEror();
                e.printStackTrace();
            } finally {
                connection.setAutoCommit(true);
            }
        } catch (SQLException e) {
            print.unableToConnectToDb();
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            print.unableFindDriver();
            e.printStackTrace();
        }
    }
}
