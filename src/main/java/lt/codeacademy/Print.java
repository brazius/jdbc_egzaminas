package lt.codeacademy;

public class Print {

    public void mainMeniuPrint() {
        System.out.println("Meniu pasirinikimas. Iveskite meniu numeri");
        System.out.println("1 - Registracija");
        System.out.println("2 - Prisjunkti");
        System.out.println("3 - Paigti darba");
    }

    public void userRegEnterId() {
        System.out.println("Iveskite asmens koda:");

    }

    public void enterName() {
        System.out.println("Iveskite varda:");
    }

    public void enterLastName() {
        System.out.println("Iveskite pavarde");
    }

    public void yourPasswordIs(String password) {
        System.out.println("Jusu slaptazodis yra: " + password);
    }

    public void unableToConnectToDb() {
        System.out.println("Unable to connect to db");
    }

    public void unableFindDriver() {
        System.out.println("Unable to find driver");
    }

    public void enterUserId() {
        System.out.println("Iveskite asmens koda: ");
    }

    public void userIdExempted() {
        System.out.println("Ivestas asmens kodas - tesingas. Iveskite slaptazodi:");
    }

    public void loginSuccess() {
        System.out.println("SLaptazodis teisingas!");
    }

    public void loginMeniu() {
        System.out.println("Pasirinkimas:");
        System.out.println("1 - Saskaitos ir ju balansas");
        System.out.println("2 - Transakciju istorija");
        System.out.println("3 - Ideti pinigus");
        System.out.println("4 - Issimti pinigus");
        System.out.println("5 - pinigu pervedimas");
        System.out.println("6 - Transakciju eksportavimas i faila");
    }

    public void moneySum() {
        System.out.println("Iveskite pinigu suma:");
    }

    public void dopositSucces() {
        System.out.println("Prinigai pervesti i saskaita sekmingai");
    }

    public void depositError() {
        System.out.println("Pinigu pervest nepavyko");
    }

    public void moneyIn() {
        System.out.println("Pinigu inesimas");
    }

    public void moneyWidraw() {
        System.out.println("Pinigu isemimas");
    }

    public void widrawOk() {
        System.out.println("Pinigus nuskaityti pavyko");
    }

    public void noMoney() {
        System.out.println("Saskaitoje nera pinigu");
    }

    public void widrawNotOk() {
        System.out.println("Pinigus nuskaityti ne pavyko" );
    }

    public void enterAccountId() {
        System.out.println("Iveskit saskaitos numeri: ");
    }

    public void acountFound() {
        System.out.println("Saskaita rasta");
    }

    public void transferEror() {
        System.out.println("Pinigu perevidimo klaida");
    }
}
