package lt.codeacademy;


import java.util.List;
import java.util.Scanner;

public class Meniu {
    Print print = new Print();
    DbManager dbManager = new DbManager();
    Scanner scanner = new Scanner(System.in);

    public void printMeniu() {
        int userChoice;
        do {
            print.mainMeniuPrint();
            userChoice = userChoiceConfig();
            menuChoice(userChoice);
        }
        while (userChoice != 3);
    }

    private void menuChoice(int userChoice) {
        if (userChoice == 1) {
            dbManager.userRegistration();
        } else if (userChoice == 2) {
            login();
        }
    }

    private void login() {
        boolean loggedIn = false;
        do {
            print.enterUserId();
            String userId = scanner.nextLine();

            List<User> users = dbManager.getAllUsers();
            List<Account> accounts = dbManager.getAllAccounts();
            for (User user : users) {
                if (userId.equals(user.getUserId())) {
                    print.userIdExempted();
                    String password = scanner.nextLine();
                    for (User user2 : users) {
                        if (password.equals(user2.getPassword())) {
                            print.loginSuccess();
                            loggedIn = true;
                            transactionWork(userId);
                            break;
                        }
                    }
                }
            }
        }
        while (!loggedIn);
    }

    private void transactionWork(String userId) {
        int userChoice;
        do {
            print.loginMeniu();
            userChoice = userChoiceConfig();
            transactionChoice(userChoice, userId);

        }
        while (userChoice !=7);
    }

    private void transactionChoice(int userChoice, String userId) {
        if (userChoice == 1) {
            DbManager dbManager = new DbManager();
            List<Account> accounts = dbManager.getAllAccounts();
            System.out.printf("%10s| %10s |%10s | %10s", "ID", "USER_ID", "ACCOUNT_TYPE", "BALANCE");
            System.out.println();
            System.out.println("-----------------------------------------------------------------------------");
            for (int i = 0; i <accounts.size(); i++) {
                if(accounts.get(i).getUserId().equals(userId)){
                    System.out.printf("%10s| %10s |%10s | %10s"
                            , accounts.get(i).getAccount_id()
                            , accounts.get(i).getUserId()
                            , accounts.get(i).getAccountType()
                            , accounts.get(i).getBalance();
                    System.out.println();
                }
            }
        } else if ( userChoice == 2) {
            // damust po transakscijos metodo
        } else if ( userChoice == 3) {
            dbManager.moneyDeposit(userId);
        } else if ( userChoice == 4) {
            dbManager.moneyWithdraw(userId);
        } else if ( userChoice == 5) {
            dbManager.moneyTransfer(userId);
        }
    }


    private int userChoiceConfig() {
        boolean hasNextInt = scanner.hasNextInt();
        int i = 0;
        if (hasNextInt) {
            i = scanner.nextInt();
        } else {
            System.out.println("Error");
        }
        return i;
    }
}
