package lt.codeacademy;

public class User {
    private String userId;
    private String name;
    private String lastName;
    private String password;


    public User(String userId, String name, String lastName, String password) {
        this.userId = userId;
        this.name = name;
        this.lastName = lastName;
        this.password = password;
    }

    public String getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }
}
